Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :boats, only: %i[index show update]

  root to: 'boats#index'
  get '/favorites', to: 'boats#favorites'
  get '/find_new_boats', to: 'boats#find_new_boats'
  get '/delete_old_boats', to: 'boats#delete_old_boats'
end
