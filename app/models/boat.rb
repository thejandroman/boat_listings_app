class Boat < ApplicationRecord
  has_paper_trail only: [:price]

  def price_history(prices = {}, boat = self)
    return price_history({ boat.updated_at => boat.price }) if prices.empty?

    older_boat = boat.paper_trail.previous_version
    return prices if older_boat.nil?

    price_history(prices.merge(older_boat.updated_at => older_boat.price), older_boat)
  end

  def listed?
    uri = URI(url)
    res = Net::HTTP.get_response(uri)
    return false unless res.code.to_i == 200

    html = Net::HTTP.get(uri)
    /The YachtWorld server has encountered an error/.match(html).nil?
  end

  OFFER_PERCENTAGE = 20
  def offer_price
    price.to_f - (price.to_f * (OFFER_PERCENTAGE / 100.0))
  end

  REGISTRATION_PRICE = 10
  def registration
    REGISTRATION_PRICE * length
  end

  TAX_PERCENTAGE = 7
  def taxes
    offer_price * (TAX_PERCENTAGE / 100.0)
  end

  INSURANCE_PERCENTAGE = 1.5
  def insurance
    offer_price * (INSURANCE_PERCENTAGE / 100.0)
  end

  def estimated_capital_expenditure
    offer_price + registration + taxes + insurance
  end
end
