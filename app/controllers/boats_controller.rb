class BoatsController < ApplicationController
  before_action :set_boat, only: %i[show update]

  def index
    respond_to do |format|
      format.html
      format.json { render json: BoatDatatable.new(params, view_context: view_context) }
    end
  end

  def show; end

  def update
    respond_to do |format|
      if @boat.update(boat_params)
        format.html { redirect_to @boat, notice: 'Boat was successfully updated.' }
        format.json { render :show, status: :ok, location: @boat }
      else
        format.html { render :show }
        format.json { render json: @boat.errors, status: :unprocessable_entity }
      end
    end
  end

  def favorites
    respond_to do |format|
      format.html
      format.json { render json: BoatDatatable.new(params, view_context: view_context, favorites: true) }
    end
  end

  def find_new_boats
    RetrieveBoatTraderBoatsJob.perform_later
    RetrieveYachtWorldBoatsJob.perform_later

    redirect_to :root, notice: 'Finding new boats...'
  end

  def delete_old_boats
    RemoveUnlistedYachtWorldBoatsJob.perform_later

    redirect_to :root, alert: 'Deleting old boats...'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_boat
    @boat = Boat.find(params[:id])
  end

  def boat_params
    params.require(:boat).permit(:yes, :no)
  end
end
