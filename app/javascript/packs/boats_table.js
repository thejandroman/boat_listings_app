require('datatables.net-bs4')(window, $)

$(document).on('turbolinks:load', function () {
  $('#boats tfoot th').each(function () {
    var title = $(this).text()
    $(this).html('<input type="text" placeholder="'+title+'" />')
  })

  var table = $('#boats').DataTable({
    processing: true,
    serverSide: true,
    searching: true,
    dom: 'lrtip',
    responsive: true,
    stateSave: true,
    ajax: {
      url: $('#boats').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'description' },
      { data: 'price' },
      { data: 'length' },
      { data: 'year' },
      { data: 'location' },
      { data: 'url' },
      { data: 'last_updated' },
      {
        data: 'yes',
        className: 'hide_column',
        createdCell: function (cell, cellData, rowData, rowIndex, colIndex) {
          if (cellData === 'true') {
            $(cell).parent().addClass('table-primary')
          }
        }
      },
      {
        data: 'no',
        className: 'hide_column',
        createdCell: function (cell, cellData, rowData, rowIndex, colIndex) {
          if (cellData === 'true') {
            $(cell).parent().addClass('table-secondary')
          }
        }
      }
    ],
    order: [[2, 'desc'], [1, 'desc']],
    initComplete: function () {
      // Apply the search
      this.api().columns().every(function () {
        var that = this

        $('input', this.footer()).on('keyup change clear', function () {
          if (that.search() !== this.value) {
            that
              .search(this.value)
              .draw()
          }
        })
      })
    }
  })

  showAllBoats(table)

  $('input:checkbox').on('change', function () {
    showAllBoats(table)
  })
})

function showAllBoats (table) {
  var checked = $('input:checkbox').is(':checked')
  var showAll = (checked ? '' : false)

  table
    .columns(8)
    .search(showAll, false)
    .draw()
}
