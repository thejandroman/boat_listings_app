# frozen_string_literal: true

# Retrieve yachtworld boats
class RetrieveYachtWorldBoatsJob < ApplicationJob
  queue_as :default

  DEFAULT_PARAMS = { length: '35-60', price: '0-50000' }.freeze
  YW_URI         = URI('https://www.yachtworld.com/boats-for-sale/condition-used/type-sail/region-northamerica/sort-price:asc/')

  def perform(*)
    i = 1

    loop do
      YW_URI.query = URI.encode_www_form(DEFAULT_PARAMS.merge(page: i))
      page_html = Net::HTTP.get(YW_URI)
      html_doc = Nokogiri::HTML(page_html)
      listings = html_doc.css('.listings-container').css('a')

      break if listings.empty?

      create_from_listings(listings)
      i += 1
    end
  end

  def create_from_listings(listings)
    parsed_listings = listings.filter_map { |listing| parse_attributes_from_listing(listing) }

    Boat.upsert_all(
      parsed_listings,
      unique_by: :sku
    )
  end

  def parse_attributes_from_listing(listing)
    price = listing.css('.price').text.scan(/\d+/).join.to_f
    return if price.blank?
    return unless price.positive?

    loa_year = listing.css('.listing-card-length-year').text.split('/')

    {
      created_at: Time.current,
      description: listing.css('.listing-card-title').text.strip,
      length: loa_year[0].strip.to_i,
      location: listing.css('.listing-card-location').text.strip,
      price: price,
      sku: listing.css('.listing-card').attribute('id').value,
      updated_at: Time.current,
      url: extract_content_from_property(listing, 'url'),
      year: loa_year[1].strip.to_i
    }
  end

  def extract_content_from_property(listing, property)
    content = listing.css('meta').find do |meta|
      meta.attribute('property').value == property
    end&.attribute('content')
    content&.value
  end
end
