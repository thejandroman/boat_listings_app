# frozen_string_literal: true

# Retrieve boattrader boats
class RetrieveBoatTraderBoatsJob < ApplicationJob
  queue_as :default

  BT_URI = URI('https://www.boattrader.com/boats/type-sail/length-35,60/price-0,50000/')

  def perform(*)
    i = 1

    loop do
      page         = "page-#{i}/"
      page_html    = Net::HTTP.get(BT_URI + page)
      html_doc     = Nokogiri::HTML(page_html)
      listing_urls = html_doc
                     .css('.boat-list')
                     .css('li[data-reporting-click-listing-type~="listing"]')
                     .css('.main-link')
                     .map { |a| a['href'] }

      break if listing_urls.empty?

      create_from_listing_urls(listing_urls)
      i += 1
    end
  end

  def create_from_listing_urls(listing_urls)
    parsed_listings = listing_urls.filter_map { |listing_url| parse_attributes_from_listing_url(listing_url) }

    Boat.upsert_all(
      parsed_listings,
      unique_by: :sku
    )
  end

  def parse_attributes_from_listing_url(listing_url)
    boat_listing = retrieve_listing_content(listing_url)

    price = boat_listing.css('.payment-total').text.gsub(/[^\d\.]/, '').to_f
    return unless price.positive?

    description = boat_listing.css('.heading').text

    {
      created_at: Time.current,
      description: description,
      length: boat_listing.css('.boat-length').text.to_i,
      location: boat_listing.css('h2.location').text,
      price: price,
      sku: listing_url.rpartition('-').last.tr('/', ''),
      updated_at: Time.current,
      url: listing_url,
      year: description.partition(' ').first.to_i
    }
  end

  def retrieve_listing_content(listing_url)
    listing_uri = URI(listing_url)
    page_html   = Net::HTTP.get(listing_uri)
    html_doc    = Nokogiri::HTML(page_html)
    html_doc.css('.content')
  end
end
