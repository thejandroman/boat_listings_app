class RemoveUnlistedYachtWorldBoatsJob < ApplicationJob
  queue_as :default

  def perform(*)
    all_older_boats = Boat.where(updated_at: ..13.week.ago)

    # Remove boats with blank price
    all_older_boats.select { |boat| boat.price.blank? }.each do |boat|
      logger.warn "Removing boat due to blank price: #{boat.id} - #{boat.description} - #{boat.price}"
      boat.destroy
    end

    # Remove boats with zero price
    all_older_boats.reject { |boat| boat.price.positive? }.each do |boat|
      logger.warn "Removing boat due to zero price: #{boat.id} - #{boat.description} - #{boat.price}"
      boat.destroy
    end

    # Remove unlisted boats
    all_older_boats.reject(&:listed?).each do |boat|
      logger.warn "Removing boat due to unlisting: #{boat.id} - #{boat.description} - #{boat.price}"
      boat.destroy
    end
  end
end
