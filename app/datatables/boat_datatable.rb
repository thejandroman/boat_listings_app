class BoatDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_currency
  def_delegator :@view, :boat_path

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      description: { source: 'Boat.description' },
      price: { source: 'Boat.price' },
      length: { source: 'Boat.length' },
      year: { source: 'Boat.year' },
      location: { source: 'Boat.location' },
      url: { source: 'Boat.url' },
      last_updated: { source: 'Boat.updated_at' },
      yes: { source: 'Boat.yes' },
      no: { source: 'Boat.no' }
    }
  end

  def data
    records.map do |record|
      {
        description: link_to(record.description, boat_path(record.id)),
        price: number_to_currency(record.price),
        length: record.length,
        year: record.year,
        location: record.location,
        url: link_to(record.url, record.url, target: '_blank', rel: 'noreferrer'),
        last_updated: record.updated_at,
        yes: record.yes,
        no: record.no,
        DT_RowId: record.id
      }
    end
  end

  def favorites
    @favorites ||= options[:favorites]
  end

  def get_raw_records
    return Boat.where(yes: true) if favorites

    Boat.all
  end
end
