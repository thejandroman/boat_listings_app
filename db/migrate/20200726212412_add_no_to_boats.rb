class AddNoToBoats < ActiveRecord::Migration[6.0]
  def change
    add_column :boats, :no, :boolean
  end
end
