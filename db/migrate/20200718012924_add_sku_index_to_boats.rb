class AddSkuIndexToBoats < ActiveRecord::Migration[6.0]
  def change
    add_index :boats, :sku, unique: true
  end
end
