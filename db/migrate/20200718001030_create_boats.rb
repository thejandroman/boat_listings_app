class CreateBoats < ActiveRecord::Migration[6.0]
  def change
    create_table :boats do |t|
      t.integer :sku
      t.string :description
      t.decimal :price
      t.integer :length
      t.integer :year
      t.string :location
      t.string :url
      t.boolean :yes
      t.boolean :ignore

      t.timestamps
    end
  end
end
