class AddDefaultValueToBooleans < ActiveRecord::Migration[6.0]
  def change
    change_column_default :boats, :yes, from: nil, to: false
    change_column_default :boats, :no, from: nil, to: false

    change_column_null :boats, :yes, false, false
    change_column_null :boats, :no, false, false
  end
end
